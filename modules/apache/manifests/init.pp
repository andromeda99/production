class apache {
file {'/root/devops.txt':                                         # resource type file and filename
      ensure  => present,                                               # make sure it exists
      mode    => '0644',                                                # file permissions
      content => template('apache/test.cfg.erb'),                      # note the ipaddress_eth0 fact
    }

$admintools = ['unzip','mlocate','net-tools','elinks','git','wget','httpd','vsftpd']

package { $admintools:
  ensure => installed,
}

$servicestart  = ['httpd','vsftpd']

service { $servicestart:
  ensure => running,
  enable => true,
}

file {'index.html':                                            # resource type file and filename
      ensure  => present,                                               # make sure it exists
      mode    => '0644',                                                # file permissions
      path    => '/var/www/html/index.html',
      content => template('apache/index.html.erb'),                          # note the ipaddress_eth0 fact
    }

}

