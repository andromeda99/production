
node 'puppetclient1.server.com' {
	include apache
}


node 'puppetclient2.server.com' {

file {'/tmp/test.sh':
      ensure  => 'present',                                               
      mode    => '0644',                                                
      content => "Hello World\n.I am Back",                     
}

file {'docker_vol':
      ensure  => 'directory',
      mode    => '0755',
      path => '/tmp/docker_vol',
}
user {'jordan':
       ensure => present,
       home => '/home/jordan',
       shell => '/bin/bash',
}

}
